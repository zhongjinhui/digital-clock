package org.awesome.clock;

public interface Observer {

    void update();

}
