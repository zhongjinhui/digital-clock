package org.awesome.clock;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    public final List<Observer> observers = new ArrayList<>();

    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    public void notifyObservers() {
        observers.forEach(Observer::update);
    }

}
