package org.awesome.clock;

public interface TimeSource {

    int getHours();

    int getMinutes();

    int getSeconds();

}

