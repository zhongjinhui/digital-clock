package org.awesome.clock;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ObserverTest {
    private MockTimeSource source;
    private MockTimeSink sink;

    @Before
    public void setUp() {
        source = new MockTimeSource();
        sink = new MockTimeSink(source);
        source.registerObserver(sink);
    }

    @Test
    public void testTimeChange() {
        source.setTime(1, 2, 3);
        assertObserverTimeEquals(sink, 1, 2, 3);

        source.setTime(4, 5, 6);
        assertObserverTimeEquals(sink, 4, 5, 6);
    }

    private void assertObserverTimeEquals(MockTimeSink sink, int hours, int minutes, int seconds) {
        assertEquals(hours, sink.getHours());
        assertEquals(minutes, sink.getMinutes());
        assertEquals(seconds, sink.getSeconds());
    }

    @Test
    public void testMultipleObserver() {
        MockTimeSink sinTwo = new MockTimeSink(source);
        source.registerObserver(sinTwo);
        source.setTime(1, 2, 3);
        assertObserverTimeEquals(sink, 1, 2, 3);
        assertObserverTimeEquals(sinTwo, 1, 2, 3);
    }

}
