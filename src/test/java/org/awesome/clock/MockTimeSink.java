package org.awesome.clock;

public class MockTimeSink implements Observer {
    private final TimeSource source;
    private int hours;
    private int minutes;
    private int seconds;

    public MockTimeSink(TimeSource source) {
        this.source = source;
    }

    int getHours() {
        return hours;
    }

    int getMinutes() {
        return minutes;
    }

    int getSeconds() {
        return seconds;
    }

    @Override
    public void update() {
        hours = source.getHours();
        minutes = source.getMinutes();
        seconds = source.getSeconds();
    }

}
